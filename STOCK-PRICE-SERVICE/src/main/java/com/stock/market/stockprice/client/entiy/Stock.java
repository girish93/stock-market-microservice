package com.stock.market.stockprice.client.entiy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer stockId;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "price")
	private Double price;
	public Stock() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Stock(Integer stockId, String companyName, Double price) {
		super();
		this.stockId = stockId;
		this.companyName = companyName;
		this.price = price;
	}
	public Integer getStockId() {
		return stockId;
	}
	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
	

}
