package com.stock.market.stockprice.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.stock.market.stockprice.client.entiy.Stock;
@Repository
public interface StockRepository extends JpaRepository<Stock, Integer>{

	@Query(value="SELECT PRICE FROM STOCK WHERE COMPANY_NAME=?1", 
			nativeQuery=true)
	public Double findPrice(String companyName);

}
