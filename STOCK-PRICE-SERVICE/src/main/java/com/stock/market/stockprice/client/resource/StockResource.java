package com.stock.market.stockprice.client.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.stock.market.stockprice.client.entiy.Stock;
import com.stock.market.stockprice.client.repository.StockRepository;

@RestController
@RequestMapping("/api-1")
public class StockResource {
	
	@Autowired
	StockRepository stockRepo;
	
	@GetMapping("/get-price/{companyName}")
	public Double getStockPrice(@PathVariable String companyName) {
		Double price = stockRepo.findPrice(companyName);
		return price;
	}
}
