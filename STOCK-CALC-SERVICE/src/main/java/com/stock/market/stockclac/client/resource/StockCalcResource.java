package com.stock.market.stockclac.client.resource;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.market.stockclac.client.entity.StockCalc;
import com.stock.market.stockclac.client.fiegnconfig.StcokCalcFiegn;

@RestController
@RequestMapping("/api-2")
public class StockCalcResource {
	
	@Autowired
	private StcokCalcFiegn calc;
	
	@GetMapping("/total-price/companyName/{companyName}/quantity/{quantity}")
	public Double getTotalPrice(@PathVariable String companyName,@PathVariable Double quantity) {
		
		  Double response = quantity*calc.getStockPrice(companyName);
		  
		  return response;
		
		
		
	}

}
