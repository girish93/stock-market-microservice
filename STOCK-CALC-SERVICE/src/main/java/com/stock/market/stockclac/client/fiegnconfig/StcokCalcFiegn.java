package com.stock.market.stockclac.client.fiegnconfig;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.stock.market.stockclac.client.entity.StockCalc;

@FeignClient(name = "STOCK-PRICE-SERVICE")
public interface StcokCalcFiegn {
	
	@GetMapping("/api-1/get-price/{companyName}")
	public Double getStockPrice(@PathVariable String companyName);

}
