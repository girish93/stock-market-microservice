package com.stock.market.stockclac.client.entity;

public class StockCalc {
	private Integer Id;
	private String companyName;
	private Double quantiy;
	private Double price;
	
	public StockCalc(Integer id, String companyName, Double quantiy, Double price) {
		super();
		Id = id;
		this.companyName = companyName;
		this.quantiy = quantiy;
		this.price = price;
	}
	 
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Double getQuantiy() {
		return quantiy;
	}
	public void setQuantiy(Double quantiy) {
		this.quantiy = quantiy;
	} 
	
	

}
